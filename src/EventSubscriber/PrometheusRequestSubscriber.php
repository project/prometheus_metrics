<?php

namespace Drupal\prometheus_metrics\EventSubscriber;

use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Event\TerminateEvent;
use Psr\Log\LoggerInterface;
use Drupal\Component\Utility\Timer;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\prometheus_metrics\Bridge\PrometheusMetricsInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Counts all requests/ total requests and provides request timer metrics.
 *
 * @package Drupal\prometheus_metrics\EventSubscriber
 */
class PrometheusRequestSubscriber implements EventSubscriberInterface {

  /**
   * Used to retrieve route details.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  private $routeMatch;

  /**
   * The prometheus metrics service.
   *
   * @var \Drupal\prometheus_metrics\Bridge\PrometheusMetricsInterface
   */
  private $prometheusMetrics;

  /**
   * Used to access Prometheus Metrics config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  private $config;

  /**
   * Namespace retrieved from config.
   *
   * @var string
   */
  private $namespace;

  /**
   * Used to log messages.
   *
   * @var \Psr\Log\LoggerInterface
   */
  private $logger;

  /**
   * List of routes to exclude from metrics
   * @var string[]
   */
  private $excludeRoutes;

  /**
   * PrometheusRequestSubscriber constructor.
   *
   * @param \Drupal\prometheus_metrics\Bridge\PrometheusMetricsInterface $prometheusMetrics
   *   The Prometheus Metrics service.
   * @param \Drupal\Core\Routing\RouteMatchInterface $routeMatch
   *   Used to retrieve route details.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Config service.
   * @param \Psr\Log\LoggerInterface $logger
   *   Logger service.
   */
  public function __construct(
    PrometheusMetricsInterface $prometheusMetrics,
    RouteMatchInterface $routeMatch,
    ConfigFactoryInterface $configFactory,
    LoggerInterface $logger
  ) {
    $this->routeMatch = $routeMatch;
    $this->prometheusMetrics = $prometheusMetrics;
    $this->config = $configFactory->get(PrometheusDefaults::CONFIGURATION_NAME);
    $this->namespace = $this->config->get('metrics_namespace') ? $this->config->get('metrics_namespace') : PrometheusDefaults::METRICS_NAMESPACE;
    $this->excludeRoutes = $this->config->get('metrics_exclude_routes') ? $this->config->get('metrics_exclude_routes') : [];
    $this->logger = $logger;
  }

  /**
   * {@inheritDoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      KernelEvents::REQUEST => ['onRequest', 9999],
      KernelEvents::TERMINATE => ['onTerminate', 0],
    ];
  }

  /**
   * Start timer for the request.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   *   The start of request event.
   */
  public function onRequest(RequestEvent $event) {
    Timer::start(PrometheusDefaults::TIMER_NAME);
  }

  /**
   * Checks if route is in list of excludes routes
   *
   * @param string $routeName
   *
   * @return boolean
   */
  private function excludeRoute(string $routeName) {
    if(!empty($this->excludeRoutes) && is_array($this->excludeRoutes))
      foreach($this->excludeRoutes as $excludeRoute){
        $regex = str_replace(['*', '/'],['([\s\S]+)','\\/'], $excludeRoute);
        if (preg_match("/^$regex$/", $routeName, $matches) === 1) {
          return true;
        }
      }
    return false;
  }

  /**
   * Handles the terminate event.
   *
   * Stores a histogram timer and counter for the request
   * by method, route and status.
   *
   * @param \Symfony\Component\HttpKernel\Event\TerminateEvent $event
   *   The event.
   */
  public function onTerminate(TerminateEvent $event) {
    if (PHP_SAPI === "cli") {
      // In CLI context there is no request to trace
      return;
    }

    Timer::stop(PrometheusDefaults::TIMER_NAME);
    $timeInMs = Timer::read(PrometheusDefaults::TIMER_NAME);
    $method = $event->getRequest()->getMethod();
    $routeName = $this->getRouteName($event->getRequest());
    if($this->excludeRoute($routeName)){
      return;
    }
    if (empty($timeInMs)) {
      $this->logger->warning(
        'Timer not found for: @method @routeName',
        [
          '@method' => $method,
          '@routeName' => $routeName
        ]
      );
    }
    $timeInS = $timeInMs / 1000;

    // Calls function to get/register counter based on namespace and name
    $counter = $this->prometheusMetrics->getCounter(
      $this->namespace,
      'http_requests_total',
      'Total number of requests',
      [
        'method',
        'route',
        'status'
      ]
    );

    $counter->inc([$method, $routeName, $this->flattenRepsonseCode($event->getResponse())]);

    // Calls function to get/register histogram based on namespace and name
    $histogram = $this->prometheusMetrics->getHistogram(
      $this->namespace,
      'http_requests_total',
      'Timer for all requests',
      [
        'method',
        'route',
        'status'
      ]
    );

    $histogram->observe($timeInS, [$method, $routeName, $this->flattenRepsonseCode($event->getResponse())]);
  }

  /**
   * Extracts the route name from the request.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   *
   * @return string|null
   *   returns null or the request uri.
   */
  private function getRouteName(Request $request): ?string {
    // Does routematch return a value
    if (!empty($this->routeMatch->getRouteName())) {
      return str_replace(".", "_", $this->routeMatch->getRouteName());
    }
    else {
      return $request->getRequestUri();
    }
  }

  /**
   * Flattens Response Status codes into 4 strings 2xx - 5xx.
   *
   * @param \Symfony\Component\HttpFoundation\Response $response
   *   The response object.
   *
   * @return int|string
   *   Returns the status as a 'grouped' string or the actual status code.
   */
  private function flattenRepsonseCode(Response $response) {
    if ($response->getStatusCode() >= 200 && $response->getStatusCode() < 300) {
      return '2xx';
    }
    elseif ($response->getStatusCode() >= 300 && $response->getStatusCode() < 400) {
      return '3xx';
    }
    elseif ($response->getStatusCode() >= 400 && $response->getStatusCode() < 500) {
      return '4xx';
    }
    elseif ($response->getStatusCode() >= 500) {
      return '5xx';
    }
    return $response->getStatusCode();
  }

}
