<?php

namespace Drupal\prometheus_metrics\Events;

use Drupal\Core\Entity\EntityInterface;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Stores details about the event.
 */
class EntityCRUDEvent extends Event {
  /**
   * The entity being affected.
   *
   * @todo// currently no value in having this. Remove/ use in logging?
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  private $entity;

  /**
   * Stores type of the entity.
   *
   * @var string
   */
  private $type;

  /**
   * Stores ID of the entity.
   *
   * @todo// currently no value in having this. Remove / use in logging?
   *
   * @var int|string|null
   */
  private $id;

  /**
   * Creates the event entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity being affected.
   */
  public function __construct(EntityInterface $entity) {
    $this->entity = $entity;
    $this->type = $this->entity->bundle();
    $this->id = $entity->id();
  }

  /**
   * Returns the type of the entity.
   *
   * @return string
   *   Entity type.
   */
  public function getType():string {
    return $this->type;
  }

}
