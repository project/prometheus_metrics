<?php

namespace Drupal\prometheus_metrics\EventSubscriber;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\prometheus_metrics\Bridge\PrometheusMetricsInterface;
use Drupal\prometheus_metrics\Events\EntityCRUDEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Provides counts by type of entity operations.
 *
 * @package Drupal\prometheus_metrics\EventSubscriber
 */
class PrometheusEntityCRUDSubscriber implements EventSubscriberInterface {
  const PROMETHEUS_ENTITY_INSERT = 'entity_insert';
  const PROMETHEUS_ENTITY_UPDATE = 'entity_update';
  const PROMETHEUS_ENTITY_DELETE = 'entity_delete';

  /**
   * The prometheus metrics service.
   *
   * @var \Drupal\prometheus_metrics\Bridge\PrometheusMetricsInterface
   */
  private $prometheusMetrics;

  /**
   * Prometheus metrics config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  private $config;

  /**
   * Prometheus metrics configured namespace.
   *
   * @var string
   */
  private $namespace;

  /**
   * Contructs the CRUD event handler.
   *
   * @param \Drupal\prometheus_metrics\Bridge\PrometheusMetricsInterface $prometheusMetrics
   *   The prometheus metrics service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Used to access prometheus metrics configuration.
   */
  public function __construct(
    PrometheusMetricsInterface $prometheusMetrics,
    ConfigFactoryInterface $configFactory
  ) {
    $this->prometheusMetrics = $prometheusMetrics;
    $this->config = $configFactory->get(PrometheusDefaults::CONFIGURATION_NAME);
    $this->namespace = $this->config->get('metrics_namespace') ? $this->config->get('metrics_namespace') : PrometheusDefaults::METRICS_NAMESPACE;
  }

  /**
   * {@inheritDoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      self::PROMETHEUS_ENTITY_INSERT => ['onEntityInsert', 0],
      self::PROMETHEUS_ENTITY_UPDATE => ['onEntityUpdate', 0],
      self::PROMETHEUS_ENTITY_DELETE => ['onEntityDelete', 0]
    ];
  }

  /**
   * On insert create/ update metrics.
   *
   * @param \Drupal\prometheus_metrics\Events\EntityCRUDEvent $event
   *   The entity crud event.
   */
  public function onEntityInsert(EntityCRUDEvent $event) {
    $counter = $this->prometheusMetrics->getCounter(
      $this->namespace,
      'entity_create',
      'Counts when entity created',
      [
        'create'
      ]
    );
    $counter->inc([$event->getType()]);
  }

  /**
   * On update create/ update metrics.
   *
   * @param \Drupal\prometheus_metrics\Events\EntityCRUDEvent $event
   *   The entity crud event.
   */
  public function onEntityUpdate(EntityCRUDEvent $event) {
    $counter = $this->prometheusMetrics->getCounter(
      $this->namespace,
      'entity_update',
      'Counts when entity updated',
      [
        'update'
      ]
    );
    $counter->inc([$event->getType()]);
  }

  /**
   * On delete create/ update metrics.
   *
   * @param \Drupal\prometheus_metrics\Events\EntityCRUDEvent $event
   *   The entity crud event.
   */
  public function onEntityDelete(EntityCRUDEvent $event) {
    $counter = $this->prometheusMetrics->getCounter(
      $this->namespace,
      'entity_delete',
      'Counts when entity deleted',
      [
        'delete'
      ]
    );
    $counter->inc([$event->getType()]);
  }

}
