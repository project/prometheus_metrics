## Prometheus Metrics

#### Introduction

This module provides a configurable endpoint to output metrics in the [Prometheus](https://prometheus.io/) text format. Prometheus is a widely used tool for monitoring key metrics of a service. With this module out of the box you can get some great insight into some metrics for your Drupal site. Such as how many entities are created/ updated and how long are requests taking.

Combining just the default data provided by this module to Prometheus with a graphing tool such as Grafana can be a great starting point for monitoring and gaining insight to how your site is being used and performing.

From the Prometheus site:

> ###### From metrics to insight
> Power your metrics and alerting with a leading
open-source monitoring solution.

This modules uses the [PromPHP/prometheus_client_php](https://packagist.org/packages/PromPHP/prometheus_client_php) to collect/ render the metrics and expose different storage options:

- Redis
- InMemory
- APC

#### Installation

Download or use composer require drupal/prometheus_metrics as you usually would with any contrib module. Depending upon which storage adapter you choose you may require some underlying php extensions to be available and enabled such as apc or redis e.g. `pecl install -o -f redis`.

#### Configuration

Default configuration will make metrics available at '/metrics', with a namespace of 'drupal' however there is a configuration page available at '/admin/config/system/prometheus that allows' you to set the endpoint for your Prometheus server to read the metrics and to define the namespace for the metrics presented by this module.

The default storage backend for this collected data is the 'in memory' storage adapter provided by the 'PromPHP/prometheus_client_php' library. However the module makes it possible to easily alter the storage type.

To alter the storage type from the default you'll need to add some settings to your sites settings.php. The possible storage type options are 'apc'/ 'redis'. 'apc' requires only the prometheus_metrics_storage_type to be defined.
```
$settings['prometheus_metrics_storage_type'] = 'redis';
```
or
```
$settings['prometheus_metrics_storage_type'] = 'apc';
```

To use redis the following additional configuration must be defined:

```
$settings['prometheus_metrics_redis_host'] = 'redis-host'
$settings['prometheus_metrics_redis_port'] = 3679;
$settings['prometheus_metrics_redis_timeout'] = 0.1;
$settings['prometheus_metrics_redis_read_timeout'] = 10;
$settings['prometheus_metrics_redis_persist_conns'] = false;
$settings['prometheus_metrics_redis_database'] = null;
```

Both the apc and redis storage adapters require the respective extension is enabled for PHP.

#### Metrics

By default, this module provides a few different metrics:

- Request timings - By route, method and status code
- Request counts - By route, method and status code
- Entity CRUD metrics - By bundle and Create/Update/Delete

Example of metrics for request timings and counts:
```
cms_http_requests_total_bucket{method="GET",route="prometheus_metrics_collect",status="2xx",le="0.005"} 11972
cms_http_requests_total_bucket{method="GET",route="prometheus_metrics_collect",status="2xx",le="0.01"} 12122
cms_http_requests_total_bucket{method="GET",route="prometheus_metrics_collect",status="2xx",le="0.025"} 14087
cms_http_requests_total_bucket{method="GET",route="prometheus_metrics_collect",status="2xx",le="0.05"} 14122
cms_http_requests_total_bucket{method="GET",route="prometheus_metrics_collect",status="2xx",le="0.075"} 14128
cms_http_requests_total_bucket{method="GET",route="prometheus_metrics_collect",status="2xx",le="0.1"} 14128
cms_http_requests_total_bucket{method="GET",route="prometheus_metrics_collect",status="2xx",le="0.25"} 14128
cms_http_requests_total_bucket{method="GET",route="prometheus_metrics_collect",status="2xx",le="0.5"} 14129
cms_http_requests_total_bucket{method="GET",route="prometheus_metrics_collect",status="2xx",le="0.75"} 14130
cms_http_requests_total_bucket{method="GET",route="prometheus_metrics_collect",status="2xx",le="1"} 14130
cms_http_requests_total_bucket{method="GET",route="prometheus_metrics_collect",status="2xx",le="2.5"} 14130
cms_http_requests_total_bucket{method="GET",route="prometheus_metrics_collect",status="2xx",le="5"} 14130
cms_http_requests_total_bucket{method="GET",route="prometheus_metrics_collect",status="2xx",le="7.5"} 14130
cms_http_requests_total_bucket{method="GET",route="prometheus_metrics_collect",status="2xx",le="10"} 14130
cms_http_requests_total_bucket{method="GET",route="prometheus_metrics_collect",status="2xx",le="+Inf"} 14130
cms_http_requests_total_count{method="GET",route="prometheus_metrics_collect",status="2xx"} 14130
cms_http_requests_total_sum{method="GET",route="prometheus_metrics_collect",status="2xx"} 35.46717
cms_http_requests_total{method="GET",route="prometheus_metrics_collect",status="2xx"} 14130
```

#### Extending metrics returned

There are likely cases where you'll want/ need more metrics reported for a bespoke project or something contributed that you use that this module doesn't cover out of the box. If you think the extension to metrics is something that the module should support by default and would likely be useful to others please feel free to raise an issue.

However if the metrics you are looking to measure are quite niche and unlikely to be of benefit to others then its time to create your own extension to this module

Guide to extending... TBC

