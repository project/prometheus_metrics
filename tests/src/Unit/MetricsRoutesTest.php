<?php

namespace Drupal\Tests\prometheus_metrics\Unit;

use Drupal\prometheus_metrics\Routing\MetricsRoutes;
use Drupal\Tests\UnitTestCase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Tests that the route is correctly returned.
 *
 * @group prometheus_metrics
 */
class MetricsRoutesTest extends UnitTestCase {
  /**
   * Test route assigned correct path based on config received.
   */
  public function testRouteReturnedCorrect() {

    $config = $this->createMock('Drupal\Core\Config\Config');
    $config->expects($this->once())->method('get')->will($this->returnValue('/testing'));
    $configFactory = $this->createMock('Drupal\Core\Config\ConfigFactoryInterface');
    $configFactory->expects($this->once())->method('get')->will($this->returnValue($config));

    // Test with a path of /testing
    $metricsRoutes = new MetricsRoutes($configFactory);
    $routeCollection = $metricsRoutes->routes();
    $this->assertInstanceOf(RouteCollection::class, $routeCollection);
    $routes = $routeCollection->all();
    $route = array_pop($routes);
    $this->assertEquals('/testing', $route->getPath());

    // Test with a path of /testing-path2
    $config = $this->createMock('Drupal\Core\Config\Config');
    $config->expects($this->once())->method('get')->will($this->returnValue('/testing-path2'));
    $configFactory = $this->createMock('Drupal\Core\Config\ConfigFactoryInterface');
    $configFactory->expects($this->once())->method('get')->will($this->returnValue($config));

    $metricsRoutes = new MetricsRoutes($configFactory);
    $routeCollection = $metricsRoutes->routes();
    $routes = $routeCollection->all();
    $route = array_pop($routes);
    $this->assertEquals('/testing-path2', $route->getPath());

    // Test with no configured path that default is correctly returned
    $config = $this->createMock('Drupal\Core\Config\Config');
    $config->expects($this->once())->method('get')->will($this->returnValue(''));
    $configFactory = $this->createMock('Drupal\Core\Config\ConfigFactoryInterface');
    $configFactory->expects($this->once())->method('get')->will($this->returnValue($config));

    $metricsRoutes = new MetricsRoutes($configFactory);
    $routeCollection = $metricsRoutes->routes();
    $routes = $routeCollection->all();
    $route = array_pop($routes);
    $this->assertEquals('/metrics', $route->getPath());
  }

}
