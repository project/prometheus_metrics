<?php

namespace Drupal\prometheus_metrics\EventSubscriber;

/**
 * Some default values used throughout the module.
 */
final class PrometheusDefaults {
  const TIMER_NAME = 'prometheus_metrics_request_timer';
  const METRICS_NAMESPACE = 'drupal';
  const CONFIGURATION_NAME = 'prometheus_metrics.configuration';
  const DEFAULT_ROUTE = "/metrics";

}
