<?php

namespace Drupal\prometheus_metrics\Controller;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\prometheus_metrics\Bridge\PrometheusMetricsInterface;
use Drupal\prometheus_metrics\EventSubscriber\PrometheusDefaults;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Controller called to render metrics.
 */
class MetricsController extends ControllerBase {
  /**
   * Interface to prometheus metrics service.
   *
   * @var \Drupal\prometheus_metrics\Bridge\PrometheusMetricsInterface
   */
  private PrometheusMetricsInterface $prometheusMetrics;

  /**
   * Null if not set, otherwise a value to pass to ini_set memory_limit.
   *
   * @var string|null
   */
  private string $route_memory_limit;

  /**
   * Constructs MetricsController object.
   *
   * @param \Drupal\prometheus_metrics\Bridge\PrometheusMetricsInterface $prometheusMetrics
   *   Interface to prometheus metrics service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Used to access prometheus metrics config.
   */
  public function __construct(PrometheusMetricsInterface $prometheusMetrics, ConfigFactoryInterface $configFactory) {
    $this->prometheusMetrics = $prometheusMetrics;
    $config = $configFactory->get(PrometheusDefaults::CONFIGURATION_NAME);
    $this->route_memory_limit = $config->get('metrics_route_memory');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('prometheus_metrics.promphp_bridge'),
      $container->get('config.factory')
    );
  }

  /**
   * Returns all metrics collected.
   *
   * @return string
   *   Return collected metrics in Prometheus format.
   */
  public function collect() {
    if (!empty($this->route_memory_limit)) {
      ini_set('memory_limit', $this->route_memory_limit);
    }
    return new Response(
      $this->prometheusMetrics->render(),
      200,
      [
        'Content-Type' => 'text/plain'
      ]
    );

  }

}
