<?php

namespace Drupal\prometheus_metrics\Routing;

use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\prometheus_metrics\EventSubscriber\PrometheusDefaults;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * Defines dynamic route to access prometheus metrics.
 */
class MetricsRoutes implements ContainerInjectionInterface {
  /**
   * Used to retrieve Prometheus Metrics config.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  private $configFactory;

  /**
   * Config defined route to metrics.
   *
   * @var string
   */
  private $metricsRoutePath;

  /**
   * MetricsRoutes constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Used retrieve Prometheus Metrics config.
   */
  public function __construct(ConfigFactoryInterface $configFactory) {
    $this->configFactory = $configFactory;
    $config = $this->configFactory->get(PrometheusDefaults::CONFIGURATION_NAME);
    $this->setMetricsRoutePath($config);
  }

  /**
   * Extracts route path from config.
   *
   * Defaults to /metrics.
   *
   * @param \Drupal\Core\Config\Config $config
   *   Config service.
   */
  private function setMetricsRoutePath(Config $config):void {
    $this->metricsRoutePath = $config->get('metrics_route_path');
    if (empty($this->metricsRoutePath)) {
      $this->metricsRoutePath = PrometheusDefaults::DEFAULT_ROUTE;
    }
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    // TODO: Implement create() method.
    return new static(
      $container->get('config.factory')
    );
  }

  /**
   * Returns route to retrieve prometheus metrics from.
   */
  public function routes() {
    $route_collection = new RouteCollection();

    $route = new Route(
    // Path to attach this route to:
      $this->metricsRoutePath,
      // Route defaults:
      [
        '_controller' => '\Drupal\prometheus_metrics\Controller\MetricsController::collect',
        '_title' => 'Collect metrics'
      ],
      // Route requirements:
      [
        '_permission'  => 'access prometheus metrics',
      ],
      [],
      '',
      [],
      // Methods:
      [
        'GET'
      ]
    );

    $route_collection->add('prometheus_metrics.collect', $route);

    // Returns a route collection object.
    return $route_collection;
  }

}
