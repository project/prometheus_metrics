<?php

namespace Drupal\Tests\prometheus_metrics\Unit;

use Drupal\prometheus_metrics\Events\EntityCRUDEvent;
use Drupal\Tests\UnitTestCase;

/**
 * Tests the EntityCRUDEvent class methods.
 *
 * @group prometheus_metrics
 */
class EntityCRUDEventTest extends UnitTestCase {
  /**
   * Tests type correctly returned.
   */
  public function testGetType() {

    $node = $this->createMock('\Drupal\node\Entity\Node');
    $node->expects($this->once())->method('bundle')->will($this->returnValue('test'));
    $entityCRUDEvent = new EntityCRUDEvent($node);
    self::assertEquals('test', $entityCRUDEvent->getType());

    $user = $this->createMock('\Drupal\user\Entity\User');
    $user->expects($this->once())->method('bundle')->will($this->returnValue('user'));
    $entityCRUDEvent = new EntityCRUDEvent($user);
    self::assertEquals('user', $entityCRUDEvent->getType());
  }

}
