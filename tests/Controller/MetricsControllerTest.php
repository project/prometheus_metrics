<?php

namespace Drupal\prometheus_metrics\Tests;

use Drupal\Tests\BrowserTestBase;

/**
 * Provides automated tests for the prometheus_metrics module.
 */
class MetricsControllerTest extends BrowserTestBase {


  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  public static function getInfo() {
    return [
      'name' => "prometheus_metrics MetricsController's controller functionality",
      'description' => 'Test Unit for module prometheus_metrics and controller MetricsController.',
      'group' => 'Other',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();
  }

  /**
   * Tests prometheus_metrics functionality.
   */
  public function testMetricsController() {
    // Check that the basic functions of module prometheus_metrics.
    $this->assertEquals(TRUE, TRUE, 'Test Unit Generated via Drupal Console.');
  }

}
