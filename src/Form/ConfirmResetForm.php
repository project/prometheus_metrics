<?php

namespace Drupal\prometheus_metrics\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\prometheus_metrics\Bridge\PrometheusMetricsInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Allows user to cancel request to wipe all metrics.
 */
class ConfirmResetForm extends ConfirmFormBase {

  /**
   * The prometheus metrics service.
   *
   * @var \Drupal\prometheus_metrics\Bridge\PrometheusMetricsInterface
   */
  private PrometheusMetricsInterface $prometheusMetrics;

  /**
   * Constructs a \Drupal\system\ConfigFormBase object.
   *
   * @param \Drupal\prometheus_metrics\Bridge\PrometheusMetricsInterface $prometheusMetrics
   *   The prometheus metrics service.
   */
  public function __construct(PrometheusMetricsInterface $prometheusMetrics) {
    $this->prometheusMetrics = $prometheusMetrics;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('prometheus_metrics.promphp_bridge'),
    );
  }

  /**
   * {@inheritDoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to wipe all metrics generated so far?');
  }

  /**
   * {@inheritDoc}
   */
  public function getCancelUrl() {
    return new Url('prometheus_metrics.configuration_form');
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return "prometheus_metrics.wipe";
  }

  /**
   * Handles submission, attempting to wipe from storage.
   *
   * @param array $form
   *   The confirm reset form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   State of confirm reset form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Delete metrics
    if ($this->prometheusMetrics->wipeRegistry()) {
      \Drupal::messenger()
        ->addMessage(
          $this->t('All metrics successfully wiped.'),
           \Drupal::messenger()::TYPE_STATUS
        );
    }
    else {
      \Drupal::messenger()
        ->addMessage(
          $this->t('Failed to wipe metrics.'),
          \Drupal::messenger()::TYPE_ERROR
            );
    }
    // Redirect back to manage form.
    $form_state->setRedirect('prometheus_metrics.configuration_form');

  }

}
