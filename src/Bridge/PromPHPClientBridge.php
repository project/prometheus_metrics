<?php

namespace Drupal\prometheus_metrics\Bridge;

use Drupal\Core\Site\Settings;
use Prometheus\CollectorRegistry;
use Prometheus\Exception\StorageException;
use Prometheus\RenderTextFormat;
use Prometheus\Storage\APC;
use Prometheus\Storage\InMemory;
use Prometheus\Storage\Redis;

/**
 * Bridge between vendor prometheus metrics library and this module.
 */
class PromPHPClientBridge implements PrometheusMetricsInterface {
  /**
   * Handles storing/ retrieving metrics.
   *
   * @var \Prometheus\Storage\Adapter
   */
  private $storageAdapter;

  /**
   * The registry populated from storage.
   *
   * @var \Prometheus\CollectorRegistry
   */
  private $collectorRegistry;

  /**
   * The renderer used to expose metrics.
   *
   * @var \Prometheus\RenderTextFormat
   */
  private $renderer;

  /**
   * Dependent upon settings.php, initializes the storage adapter.
   *
   * Other services, then initialized for that storage adapter.
   */
  public function __construct() {
    $storageType = Settings::get('prometheus_metrics_storage_type');
    switch ($storageType) {
      case 'redis':
        $options = [
          'host' => Settings::get('prometheus_metrics_redis_host'),
          'port' => Settings::get('prometheus_metrics_redis_port'),
          'timeout' => Settings::get('prometheus_metrics_redis_timeout'),
          'read_timeout' => Settings::get('prometheus_metrics_redis_read_timeout'),
          'persistent_connections' => Settings::get('prometheus_metrics_redis_persist_conns'),
          'password' => NULL,
          'database' => Settings::get('prometheus_metrics_redis_database')
        ];
        $this->storageAdapter = new Redis($options);
        break;
      case 'apc':
        $this->storageAdapter = new APC();
        break;
      default:
        // Default to in memory storage adapter
        $this->storageAdapter = new InMemory();
        break;
    }
    try {
      $this->collectorRegistry = new CollectorRegistry($this->storageAdapter);
      $this->renderer = new RenderTextFormat();
    } catch (\Exception $e){
      throw new \BadMethodCallException(
        sprintf(
          'Unable to setup collector. Please review your settings.php config for %s. Original error: %s',
          $storageType,
          $e->getMessage()
        )
      );
    }
  }

  /**
   * Returns the renderer.
   *
   * @return \Prometheus\RenderTextFormat
   *   Returns a Text format renderer.
   */
  private function getRenderer(): RenderTextFormat {
    return $this->renderer;
  }

  /**
   * Returns the registry.
   *
   * @return \Prometheus\CollectorRegistry
   *   Returns the registry.
   */
  private function getCollectorRegistry(): CollectorRegistry {
    return $this->collectorRegistry;
  }

  /**
   * {@inheritDoc}
   */
  public function wipeRegistry(): bool {
    try {
      $this->storageAdapter->wipeStorage();
      return TRUE;
    }
    catch (StorageException $e) {
      // @todo log failure.
      return FALSE;
    }
  }

  /**
   * {@inheritDoc}
   */
  public function render(): string {
    return $this->getRenderer()
      ->render(
        $this->getCollectorRegistry()->getMetricFamilySamples()
      );
  }

  /**
   * {@inheritDoc}
   */
  public function getCounter(string $namespace, string $name, string $help, array $labels) {
    return $this->getCollectorRegistry()->getOrRegisterCounter(
      $namespace,
      $name,
      $help,
      $labels
    );
  }

  /**
   * {@inheritDoc}
   */
  public function getHistogram(string $namespace, string $name, string $help, array $labels) {
    return $this->getCollectorRegistry()->getOrRegisterHistogram(
      $namespace,
      $name,
      $help,
      $labels
    );
  }

}
