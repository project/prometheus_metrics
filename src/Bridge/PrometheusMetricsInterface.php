<?php

namespace Drupal\prometheus_metrics\Bridge;

/**
 * Interface for service passed into handle prometheus metrics.
 */
interface PrometheusMetricsInterface {

  /**
   * Handles rendering of the metrics.
   *
   * Should return compliant prometheus metrics format.
   *
   * @return string
   *   The output of rendering in text format.
   */
  public function render(): string;

  /**
   * Returns a counter collector.
   *
   * @param string $namespace
   *   Namespace for counter.
   * @param string $name
   *   Name of counter.
   * @param string $help
   *   They help text.
   * @param array $labels
   *   Any labels to be associated to count.
   *
   * @return mixed
   *   Returns a collector.
   */
  public function getCounter(string $namespace, string $name, string $help, array $labels);

  /**
   * Returns a histogram collector.
   *
   * @param string $namespace
   *   Namespace for counter.
   * @param string $name
   *   Name of counter.
   * @param string $help
   *   They help text.
   * @param array $labels
   *   Any labels to be associated to count.
   *
   * @return mixed
   *   Returns a collector.
   */
  public function getHistogram(string $namespace, string $name, string $help, array $labels);

  /**
   * Wipes the registry clean.
   *
   * Should ensure all metrics deleted.
   *
   * @return bool
   *   returns true on success, and false on fail.
   */
  public function wipeRegistry(): bool;
  //  Public function getGauge();
  //  public function getSummary();
}
